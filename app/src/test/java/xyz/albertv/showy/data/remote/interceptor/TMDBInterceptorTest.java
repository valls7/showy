package xyz.albertv.showy.data.remote.interceptor;

import org.junit.Test;

import java.io.IOException;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * TMDBInterceptor.
 */

public class TMDBInterceptorTest {

    @Test(expected = IllegalArgumentException.class)
    public void nullParamIsNotAllowed() {
        new TMDBInterceptor(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyParamIsNotAllowed() {
        new TMDBInterceptor("");
    }

    @Test
    public void stringExpectedAsAKey() {
        new TMDBInterceptor("keytest");
    }

    @Test
    public void apiKeyShouldBeOnRequestQueryParams() throws IOException, InterruptedException {
        TMDBInterceptor interceptor = new TMDBInterceptor("keytest");
        MockWebServer mockWebServer = new MockWebServer();
        mockWebServer.start();
        mockWebServer.enqueue(new MockResponse());

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(interceptor).build();
        okHttpClient.newCall(new Request.Builder().url(mockWebServer.url("/")).build()).execute();

        RecordedRequest request = mockWebServer.takeRequest();
        assertTrue(request.getRequestUrl().query().contains("api_key=keytest"));
        mockWebServer.shutdown();
    }
}
