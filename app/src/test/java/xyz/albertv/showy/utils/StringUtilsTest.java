package xyz.albertv.showy.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * StringUtilsTest.
 */

public class StringUtilsTest {

    @Test
    public void assert_emptyString() {
        assertEquals(StringUtils.isNotNullOrEmpty(""), false);
    }

    @Test
    public void assert_nullString() {
        assertEquals(StringUtils.isNotNullOrEmpty(null), false);
    }

    @Test
    public void assert_containsString() {
        assertEquals(StringUtils.isNotNullOrEmpty("hello"), true);
    }
}
