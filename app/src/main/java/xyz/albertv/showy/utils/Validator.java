package xyz.albertv.showy.utils;

import java.io.InvalidObjectException;

/**
 * Validator.
 */

public abstract class Validator<T> {

    protected abstract boolean hasRequiredFields(T object);

    public void validate(T object) throws InvalidObjectException {
        if (!hasRequiredFields(object)) {
            throw new InvalidObjectException("Missing fields on the given object" + object.getClass().getSimpleName());
        }
    }
}
