package xyz.albertv.showy.utils;

import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;

/**
 * AbstractMapper.
 */

public abstract class AbstractMapper<T, K> {

    public abstract T map(K object) throws InvalidObjectException;
    public abstract K reverseMap(T object) throws InvalidObjectException;

    public List<T> map(List<K> objects) throws InvalidObjectException {
        List<T> objectsMapped = new ArrayList<T>();
        for (int i = 0; i < objects.size(); i++) {
            objectsMapped.add(map(objects.get(i)));
        }
        return objectsMapped;
    }

    public List<K> reverseMap(List<T> objects) throws InvalidObjectException {
        List<K> objectsMapped = new ArrayList<K>();
        for (int i = 0; i < objects.size(); i++) {
            objectsMapped.add(reverseMap(objects.get(i)));
        }
        return objectsMapped;
    }
}
