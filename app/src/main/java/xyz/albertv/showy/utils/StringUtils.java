package xyz.albertv.showy.utils;

/**
 * StringUtils.
 */

public final class StringUtils {

    public static boolean isNotNullOrEmpty(String string) {
        return string != null && !string.isEmpty();
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }
}
