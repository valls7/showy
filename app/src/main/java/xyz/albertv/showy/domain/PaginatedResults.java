package xyz.albertv.showy.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * PaginatedResults.
 */

public class PaginatedResults<T> {

    private Integer page;
    private List<T> results = new ArrayList<>();
    private Integer totalResults;
    private Integer totalPages;

    private PaginatedResults(Builder builder) {
        page = builder.page;
        results = builder.results;
        totalResults = builder.totalResults;
        totalPages = builder.totalPages;
    }


    public Integer getPage() {
        return page;
    }

    public List<T> getResults() {
        return results;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }


    public static final class Builder<T> {
        private Integer page;
        private List<T> results;
        private Integer totalResults;
        private Integer totalPages;

        public Builder() {
        }

        public Builder page(Integer val) {
            page = val;
            return this;
        }

        public Builder results(List<T> val) {
            results = val;
            return this;
        }

        public Builder totalResults(Integer val) {
            totalResults = val;
            return this;
        }

        public Builder totalPages(Integer val) {
            totalPages = val;
            return this;
        }

        public PaginatedResults build() {
            return new PaginatedResults(this);
        }
    }
}
