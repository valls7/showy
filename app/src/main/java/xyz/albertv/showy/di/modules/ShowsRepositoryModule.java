package xyz.albertv.showy.di.modules;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import xyz.albertv.showy.data.Remote;
import xyz.albertv.showy.data.ShowsDataSource;
import xyz.albertv.showy.data.ShowsRepository;
import xyz.albertv.showy.data.remote.ShowsRemoteDataSource;
import xyz.albertv.showy.data.remote.entity.ShowEntity;
import xyz.albertv.showy.data.remote.entity.mapper.PaginatedResultsEntityMapper;
import xyz.albertv.showy.data.remote.entity.mapper.ShowEntityMapper;
import xyz.albertv.showy.domain.Show;

/**
 * ShowsRepositoryModules.
 */

@Module
abstract public class ShowsRepositoryModule {

    @Singleton
    @Provides
    static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    @Provides
    static PaginatedResultsEntityMapper<Show, ShowEntity> providePaginatedResultsEntityMapper(ShowEntityMapper showEntityMapper) {
        return new PaginatedResultsEntityMapper<Show, ShowEntity>(showEntityMapper);
    }

    @Singleton
    @Binds
    @Remote
    public abstract ShowsDataSource provideShowsRemoteDataSource(ShowsRemoteDataSource dataSource);

    @Singleton
    @Binds
    public abstract ShowsDataSource provideShowsDataSource(ShowsRepository showsRepository);
}
