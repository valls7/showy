package xyz.albertv.showy.di.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import xyz.albertv.showy.di.PerActivity;
import xyz.albertv.showy.presentation.showlist.ShowsListActivity;

/**
 * ActivityBindingsModule.
 */

@Module
public abstract class ActivityBindingsModule {
    @PerActivity
    @ContributesAndroidInjector(modules = ShowListActivityModule.class)
    abstract ShowsListActivity showListActivity();
}
