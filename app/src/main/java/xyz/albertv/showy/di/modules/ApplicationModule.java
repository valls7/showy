package xyz.albertv.showy.di.modules;

import android.content.Context;

import dagger.Binds;
import dagger.Module;
import xyz.albertv.showy.presentation.ShowyApplication;

/**
 * ApplicationModule.
 */

@Module
public abstract class ApplicationModule {

    // same as provides but this returns injected parameter
    @Binds
    abstract Context bindContext(ShowyApplication application);

}