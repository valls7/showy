package xyz.albertv.showy.di;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * PerActivity.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
