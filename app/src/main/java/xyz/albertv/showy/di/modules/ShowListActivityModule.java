package xyz.albertv.showy.di.modules;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import xyz.albertv.showy.data.ShowsRepository;
import xyz.albertv.showy.presentation.common.ImageLoader;
import xyz.albertv.showy.presentation.showlist.ShowsAdapter;
import xyz.albertv.showy.presentation.showlist.ShowsListActivity;
import xyz.albertv.showy.presentation.showlist.ShowsListContract;
import xyz.albertv.showy.presentation.showlist.ShowsListPresenter;

/**
 * MainActivityModule.
 */

@Module
public abstract class ShowListActivityModule {

    @Provides
    static ShowsListContract.Presenter providePresenter(ShowsListContract.View view, ShowsRepository repository) {
        return new ShowsListPresenter(view, repository);
    }

    @Provides
    static ImageLoader provideImageLoader(ShowsListActivity activity) {
        return new ImageLoader(activity);
    }

    @Provides
    static ShowsAdapter provideShowsAdapter(ImageLoader imageLoader) {
        return new ShowsAdapter(imageLoader);
    }

    @Binds
    public abstract ShowsListContract.View view(ShowsListActivity activity);

}
