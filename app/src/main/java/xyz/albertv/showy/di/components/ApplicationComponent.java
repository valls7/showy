package xyz.albertv.showy.di.components;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import xyz.albertv.showy.data.ShowsRepository;
import xyz.albertv.showy.di.modules.ActivityBindingsModule;
import xyz.albertv.showy.di.modules.ApplicationModule;
import xyz.albertv.showy.di.modules.ShowsRepositoryModule;
import xyz.albertv.showy.presentation.ShowyApplication;

/**
 * ApplicationComponent.
 */

@Singleton
@Component(modules = {ShowsRepositoryModule.class, AndroidSupportInjectionModule.class, ApplicationModule.class, ActivityBindingsModule.class})
public interface ApplicationComponent extends AndroidInjector<ShowyApplication> {

    ShowsRepository getShowsRepository();

    @Component.Builder interface Builder {

        @BindsInstance
        ApplicationComponent.Builder application(ShowyApplication application);

        ApplicationComponent build();
    }
}
