package xyz.albertv.showy.presentation;

import android.support.annotation.VisibleForTesting;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import xyz.albertv.showy.data.ShowsRepository;
import xyz.albertv.showy.di.components.DaggerApplicationComponent;

/**
 * ShowyApplication.
 */

public class ShowyApplication extends DaggerApplication {

    @Inject
    ShowsRepository showsRepository;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().application(this).build();
    }

    @VisibleForTesting
    public ShowsRepository getShowsRepository() {
        return showsRepository;
    }
}
