package xyz.albertv.showy.presentation.model;

/**
 * ShowViewModel.
 */

public class ShowViewModel {

    private final int id;
    private final String title;
    private final String posterPath;
    private final String backdropPath;
    private final String voteAverage;

    private ShowViewModel(Builder builder) {
        id = builder.id;
        title = builder.title;
        posterPath = builder.posterPath;
        backdropPath = builder.backdropPath;
        voteAverage = builder.voteAverage;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public String getVoteAverage() {
        return voteAverage;
    }

    public static final class Builder {
        private int id;
        private String title;
        private String posterPath;
        private String backdropPath;
        private String voteAverage;

        public Builder() {
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder posterPath(String val) {
            posterPath = val;
            return this;
        }

        public Builder backdropPath(String val) {
            backdropPath = val;
            return this;
        }

        public Builder voteAverage(String val) {
            voteAverage = val;
            return this;
        }

        public ShowViewModel build() {
            return new ShowViewModel(this);
        }
    }
}
