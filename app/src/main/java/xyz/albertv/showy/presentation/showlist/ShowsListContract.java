package xyz.albertv.showy.presentation.showlist;

import java.util.List;

import xyz.albertv.showy.presentation.BasePresenter;
import xyz.albertv.showy.presentation.BaseView;
import xyz.albertv.showy.presentation.model.ShowViewModel;

/**
 * ShowsListContract.
 */

public interface ShowsListContract {

    interface View extends BaseView<BasePresenter> {

        void showList(List<ShowViewModel> shows);
    }

    interface Presenter extends BasePresenter<View> {

        void loadData();
    }
}
