package xyz.albertv.showy.presentation.common;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * ImageLoader.
 */

public class ImageLoader {

    private Context context;

    public ImageLoader(Context context) {
        this.context = context;
    }

    public void load(final ImageView imageView, final String url) {
        Glide.with(context)
                .load(url)
                .into(imageView);
        //TODO add fade in animation
    }
}
