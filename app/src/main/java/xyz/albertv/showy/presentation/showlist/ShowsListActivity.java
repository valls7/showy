package xyz.albertv.showy.presentation.showlist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.DaggerActivity;
import xyz.albertv.showy.R;
import xyz.albertv.showy.presentation.model.ShowViewModel;

/**
 * ShowsListActivity.
 */

public class ShowsListActivity extends DaggerActivity implements ShowsListContract.View {

    @Inject ShowsListContract.Presenter presenter;
    @Inject ShowsAdapter showsAdapter;
    @BindView(R.id.show_list_recycler_view) RecyclerView showsRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);
        initViews();
        presenter.bindView(this);
        presenter.loadData();
    }

    private void initViews() {
        ButterKnife.bind(this);
        this.showsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.showsRecyclerView.setAdapter(showsAdapter); //TODO add pagination to the adapter
    }

    @Override
    protected void onDestroy() {
        presenter.unbindView();
        super.onDestroy();
    }

    @Override
    public void showList(List<ShowViewModel> shows) {
        showsAdapter.addAll(shows);
    }

    @Override
    public void showError(Throwable e) {

    }
}
