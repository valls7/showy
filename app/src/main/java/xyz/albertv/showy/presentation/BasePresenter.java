package xyz.albertv.showy.presentation;

/**
 * BasePresenter.
 */

public interface BasePresenter<T extends BaseView> {

    void bindView(T view);

    void unbindView();
}
