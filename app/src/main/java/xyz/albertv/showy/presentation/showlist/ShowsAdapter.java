package xyz.albertv.showy.presentation.showlist;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.albertv.showy.R;
import xyz.albertv.showy.presentation.common.ImageLoader;
import xyz.albertv.showy.presentation.model.ShowViewModel;

/**
 * ShowsAdapter.
 */

public class ShowsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int SHOW = 0;

    private List<ShowViewModel> shows = new ArrayList<>();
    private ImageLoader imageLoader;

    public ShowsAdapter(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
        this.shows = new ArrayList<>();
    }

    public List<ShowViewModel> getShows() {
        return shows;
    }

    public void setShows(List<ShowViewModel> shows) {
        this.shows = shows;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case SHOW:
                viewHolder = createShowViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder createShowViewHolder(ViewGroup parent, LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.recycler_show_list_element, parent, false);
        return new ShowViewHolder(view, imageLoader);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ShowViewModel show = shows.get(position);
        switch (getItemViewType(position)) {
            case SHOW:
                final ShowViewHolder showViewHolder = (ShowViewHolder) holder;
                showViewHolder.setShow(show);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return shows == null ? 0 : shows.size();
    }

    @Override
    public int getItemViewType(int position) {
        return SHOW;
    }

    public void add(ShowViewModel show) {
        shows.add(show);
        notifyItemInserted(shows.size() - 1);
    }

    public void addAll(List<ShowViewModel> shows) {
        int positionStart = this.shows.size();
        int itemCount = shows.size();
        this.shows.addAll(shows);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public ShowViewModel getItem(int position) {
        return shows.get(position);
    }

    protected class ShowViewHolder extends RecyclerView.ViewHolder {

        private final ImageLoader imageLoader;
        @BindView(R.id.recycler_show_list_title) TextView showTitle;
        @BindView(R.id.recycler_show_list_vote_average) TextView showVoteAverage;
        @BindView(R.id.recycler_show_list_image_view) ImageView showImage;

        public ShowViewHolder(View itemView, ImageLoader imageLoader) {
            super(itemView);
            this.imageLoader = imageLoader;
            ButterKnife.bind(this, itemView);
        }

        public void setShow(ShowViewModel show) {
            this.showTitle.setText(show.getTitle());
            this.showVoteAverage.setText(show.getVoteAverage());
            this.imageLoader.load(this.showImage, show.getBackdropPath());
        }
    }

}