package xyz.albertv.showy.presentation.error;

import android.support.annotation.StringRes;
import android.util.Log;

import java.io.InvalidObjectException;

import xyz.albertv.showy.R;

/**
 * ErrorManager.
 */

public class ErrorManager {

    //TODO inject on every activity
    private static final String TAG = "ErrorManager";
    //private ErrorView errorView;

    public ErrorManager() {
        //this.errorView = errorView;
    }

    //TODO add snack bar
    public void show(Throwable e) {
        Log.w(TAG, "Showing error", e);
        //if (errorView != null) {
        //    errorView.setText(transformUserFriendly(e));
        //}
    }

    @StringRes
    private int transformUserFriendly(Throwable e) {
        if (e instanceof InvalidObjectException) {
            return R.string.error_missing_required_fields;
        } else {
            return R.string.error_generic;
        }
    }
}
