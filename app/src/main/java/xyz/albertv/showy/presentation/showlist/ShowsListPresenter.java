package xyz.albertv.showy.presentation.showlist;

import java.util.List;

import javax.annotation.Nullable;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import xyz.albertv.showy.data.ShowsRepository;
import xyz.albertv.showy.presentation.mapper.ShowViewModelMapper;
import xyz.albertv.showy.presentation.model.ShowViewModel;

/**
 * ShowsListPresenter.
 */

public class ShowsListPresenter implements ShowsListContract.Presenter {

    @Nullable
    private ShowsListContract.View view;
    private ShowsRepository repository;

    public ShowsListPresenter(ShowsListContract.View view, ShowsRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void loadData() {
        repository.getMostPopularTVShows()
                .map(result -> new ShowViewModelMapper().map(result.getResults()))
                .subscribeOn(Schedulers.io()) //TODO move schedulers to AppModule
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<ShowViewModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<ShowViewModel> shows) {
                        view.showList(shows);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void bindView(ShowsListContract.View view) {
        this.view = view;
    }

    @Override
    public void unbindView() {
        this.view = null;
    }
}
