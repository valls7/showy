package xyz.albertv.showy.presentation.mapper;

import android.annotation.SuppressLint;

import java.io.InvalidObjectException;
import java.text.DecimalFormat;

import javax.inject.Inject;

import xyz.albertv.showy.data.remote.entity.ShowEntity;
import xyz.albertv.showy.data.remote.entity.validator.ShowEntityValidator;
import xyz.albertv.showy.domain.Show;
import xyz.albertv.showy.presentation.model.ShowViewModel;
import xyz.albertv.showy.utils.AbstractMapper;

/**
 * ShowEntityMapper.
 */

public class ShowViewModelMapper extends AbstractMapper<ShowViewModel, Show> {


    @Inject
    public ShowViewModelMapper() {
    }

    @SuppressLint("DefaultLocale")
    @Override
    public ShowViewModel map(Show object) throws InvalidObjectException {
        return new ShowViewModel.Builder()
                .id(object.getId())
                .title(object.getTitle())
                .voteAverage(String.format( "%.1f", object.getVoteAverage()))
                .posterPath(object.getPosterPath())
                .backdropPath(object.getBackdropPath())
                .build();
    }

    @Override
    public Show reverseMap(ShowViewModel object) throws InvalidObjectException {
        throw new UnsupportedOperationException("Not implemented exception");
    }
}
