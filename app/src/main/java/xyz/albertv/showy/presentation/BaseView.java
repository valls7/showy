package xyz.albertv.showy.presentation;

/**
 * BaseView.
 */

public interface BaseView<T> {

    void showError(Throwable e);
}
