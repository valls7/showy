package xyz.albertv.showy.data;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import xyz.albertv.showy.domain.PaginatedResults;
import xyz.albertv.showy.domain.Show;

/**
 * ShowsRepository.
 */

@Singleton
public class ShowsRepository implements ShowsDataSource {

    private final ShowsDataSource showsRemoteDataSource;

    @Inject
    public ShowsRepository(@Remote ShowsDataSource showsRemoteDataSource) {
        this.showsRemoteDataSource = showsRemoteDataSource;
    }

    @Override
    public Observable<PaginatedResults<Show>> getMostPopularTVShows() {
        return this.showsRemoteDataSource.getMostPopularTVShows();
    }
}
