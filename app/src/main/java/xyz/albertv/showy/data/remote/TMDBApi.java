package xyz.albertv.showy.data.remote;

import io.reactivex.Observable;
import retrofit2.http.GET;
import xyz.albertv.showy.data.remote.entity.PaginatedResultsEntity;
import xyz.albertv.showy.data.remote.entity.ShowEntity;

/**
 * TMDBApi.
 */

public interface TMDBApi {

    String ENDPOINT = "https://api.themoviedb.org/3/";
    String ENDPOINT_IMAGES = "https://image.tmdb.org/t/p/w600";
    String TOKEN = "d09e188304e8e8825cc260f24d0351a6";

    @GET("tv/popular")
    Observable<PaginatedResultsEntity<ShowEntity>> getMostPopularTVShows();
}
