package xyz.albertv.showy.data.remote.entity.validator;

import javax.inject.Inject;

import xyz.albertv.showy.data.remote.entity.ShowEntity;
import xyz.albertv.showy.utils.StringUtils;
import xyz.albertv.showy.utils.Validator;

/**
 * ShowEntityValidator.
 */

public class ShowEntityValidator extends Validator<ShowEntity> {

    @Inject
    public ShowEntityValidator() {}

    @Override
    protected boolean hasRequiredFields(ShowEntity object) {
        return object.getId() != null &&
                StringUtils.isNotNullOrEmpty(object.getTitle()) &&
                object.getVoteAverage() != null;
    }
}
