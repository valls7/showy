package xyz.albertv.showy.data.remote.entity.mapper;

import java.io.InvalidObjectException;

import javax.inject.Inject;

import xyz.albertv.showy.data.remote.entity.PaginatedResultsEntity;
import xyz.albertv.showy.domain.PaginatedResults;
import xyz.albertv.showy.utils.AbstractMapper;

/**
 * PaginatedResultsEntityMapper.
 */

public class PaginatedResultsEntityMapper<T, K> extends AbstractMapper<PaginatedResults<T>, PaginatedResultsEntity<K>> {

    private final AbstractMapper<T, K> mapper;

    public PaginatedResultsEntityMapper(AbstractMapper<T, K> mapper) {
        this.mapper = mapper;
    }

    @SuppressWarnings("unchecked")
    @Override
    public PaginatedResults<T> map(PaginatedResultsEntity<K> object) throws InvalidObjectException {
        return new PaginatedResults.Builder<T>()
                .page(object.getPage())
                .totalPages(object.getTotalPages())
                .results(mapper.map(object.getResults()))
                .totalResults(object.getTotalPages())
                .build();
    }

    @Override
    public PaginatedResultsEntity<K> reverseMap(PaginatedResults<T> object) throws InvalidObjectException {
        throw new UnsupportedOperationException("Not implemented exception");
    }
}
