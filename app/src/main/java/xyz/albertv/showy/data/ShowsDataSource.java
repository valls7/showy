package xyz.albertv.showy.data;

import io.reactivex.Observable;
import xyz.albertv.showy.domain.PaginatedResults;
import xyz.albertv.showy.domain.Show;

/**
 * ShowsDataSource.
 */

public interface ShowsDataSource {

    Observable<PaginatedResults<Show>> getMostPopularTVShows();
}
