package xyz.albertv.showy.data.remote.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import xyz.albertv.showy.utils.StringUtils;

/**
 * TMDBInterceptor.
 */

public class TMDBInterceptor implements Interceptor {

    private static final String HEADER_API_KEY = "api_key";
    private final String apiKey;

    public TMDBInterceptor(@NonNull String apiKey) {
        if (StringUtils.isNullOrEmpty(apiKey)) {
            throw new IllegalArgumentException("You must provide an Api Key in order to connect to TMDB");
        }
        this.apiKey = apiKey;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();

        if (StringUtils.isNotNullOrEmpty(apiKey)) {
            HttpUrl url = request.url().newBuilder().addQueryParameter(HEADER_API_KEY, apiKey).build();
            request = request.newBuilder().url(url).build();
        }
        return chain.proceed(request);
    }
}
