package xyz.albertv.showy.data.remote;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import xyz.albertv.showy.data.ShowsDataSource;
import xyz.albertv.showy.data.remote.entity.ShowEntity;
import xyz.albertv.showy.data.remote.entity.mapper.PaginatedResultsEntityMapper;
import xyz.albertv.showy.data.remote.interceptor.TMDBInterceptor;
import xyz.albertv.showy.domain.PaginatedResults;
import xyz.albertv.showy.domain.Show;

import static xyz.albertv.showy.data.remote.TMDBApi.ENDPOINT;

/**
 * ShowsRemoteDataSource.
 */

@Singleton
public class ShowsRemoteDataSource implements ShowsDataSource {

    private final TMDBApi tmdbApi;

    //TODO this is not working, see other approach to do that...
    @Inject
    PaginatedResultsEntityMapper<Show, ShowEntity> paginatedResultsEntityMapper;

    @Inject
    public ShowsRemoteDataSource(OkHttpClient client) {
        OkHttpClient.Builder okHttpClientBuilder = client.newBuilder();
        okHttpClientBuilder.addInterceptor(new TMDBInterceptor(TMDBApi.TOKEN));
        Retrofit marvelApiAdapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create()) //TODO Add custom gson instance
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClientBuilder.build())
                .build();

        tmdbApi = marvelApiAdapter.create(TMDBApi.class);
    }

    public Observable<PaginatedResults<Show>> getMostPopularTVShows() {
        return tmdbApi.getMostPopularTVShows().map(paginatedResultsEntityMapper::map);
    }
}
