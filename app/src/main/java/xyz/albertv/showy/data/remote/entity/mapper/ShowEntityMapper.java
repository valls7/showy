package xyz.albertv.showy.data.remote.entity.mapper;

import java.io.InvalidObjectException;

import javax.inject.Inject;

import xyz.albertv.showy.data.remote.TMDBApi;
import xyz.albertv.showy.data.remote.entity.ShowEntity;
import xyz.albertv.showy.data.remote.entity.validator.ShowEntityValidator;
import xyz.albertv.showy.domain.Show;
import xyz.albertv.showy.utils.AbstractMapper;

/**
 * ShowEntityMapper.
 */

public class ShowEntityMapper extends AbstractMapper<Show, ShowEntity> {

    @Inject
    ShowEntityValidator showEntityValidator;

    @Inject
    public ShowEntityMapper() {}

    @Override
    public Show map(ShowEntity object) throws InvalidObjectException {
        this.showEntityValidator.validate(object);
        return new Show.Builder()
                .id(object.getId())
                .title(object.getTitle())
                .voteAverage(object.getVoteAverage())
                .posterPath(TMDBApi.ENDPOINT_IMAGES + object.getPosterPath())
                .backdropPath(TMDBApi.ENDPOINT_IMAGES + object.getBackdropPath())
                .build();
    }

    @Override
    public ShowEntity reverseMap(Show object) throws InvalidObjectException {
        throw new UnsupportedOperationException("Not implemented exception");
    }
}
